
#ifdef __CINT__

#pragma link C++ class TStar+;                   // Collection of static fields and functions
#pragma link C++ class TStConfig+;               // Star configuration
#pragma link C++ class TStRun+;                  // Star configuration
#pragma link C++ class TStRunList+;              // Star configuration
#pragma link C++ class TStTrigDef+;              // Trigger Definition Reader
#pragma link C++ class TStScheduler+;            // Bright-STAR implementation of Scheduler
// #pragma link C++ function help;                  // Helper function
// #pragma link C++ function history;               // Print Command History

#endif
