// Filename: EjAnalysisTreeQa.h
// Description: 
// Author: Latif Kabir < kabir@bnl.gov >
// Created: Sat May  9 14:01:48 2020 (-0400)
// URL: jlab.org/~latif

#include "TString.h"

void EjAnalysisTreeQa(TString inFileName, TString outName = "EjAnalysisTreeQa.root");
