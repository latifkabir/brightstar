// Filename: AnResubmitJobs.h
// Description: 
// Author: Latif Kabir < kabir@bnl.gov >
// Created: Sat Dec  7 01:04:58 2019 (-0500)
// URL: jlab.org/~latif

void AnResubmitJobs(TString functionName, Int_t firstRun,  Int_t lastRunOrNfiles, TString outName, TString jobName);
